var gulp = require('gulp'), // gulp
  sass = require('gulp-sass'), // sass
  autoprefixer = require('gulp-autoprefixer'),
  fileinclude = require('gulp-file-include'), // html
  concat = require('gulp-concat'),
  jshint = require('gulp-jshint'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  sourcemaps = require('gulp-sourcemaps'),
  babel = require('gulp-babel'),
  wait = require('gulp-wait'),

  pipeline = require('readable-stream').pipeline,
  browserSync = require('browser-sync').create();

var paths = {
  src: './src',
  dist: './dist',

  DistCss: './dist/css',
  DistJs: './dist/js',
  DistFonts: './dist/fonts',
  DistImg: './dist/img',
  DistLib: './dist/lib',
  DistVendor: './dist/plugin',

  views: './src/views',
  scss: './src/scss',
  js: './src/js',
  vendor: './src/plugin',

  node: './node_modules'
};

// jQuery
gulp.task('copy_assets', () => {

  gulp.src(paths.node + '/font-awesome/fonts/*.{ttf,woff,woff2,eot,svg}')
    .pipe(gulp.dest(paths.DistFonts));

  return pipeline(
    gulp.src(paths.node + '/jquery/dist/*'),
    gulp.dest(paths.DistLib + '/jquery')
  );
});

// html
gulp.task('htmlinclude', () => {
  return pipeline(
    gulp.src([paths.views + '/*.html']),
    fileinclude({
      prefix: '@@',
      basepath: '@file'
    }),
    gulp.dest(paths.dist)
  );
});

// sass
gulp.task('sass', () => {
  return pipeline(
    gulp.src([
      paths.scss + '/*.scss'
    ])
      .pipe(wait(1000))
      .pipe(sass({
        outputStyle: 'expanded',
        indentType: 'space',
        indentWidth: 2
      }).on('error', sass.logError))
      .pipe(autoprefixer({
        browsers: ['last 10 versions']
      }))
      .pipe(gulp.dest(paths.DistCss)),
    sourcemaps.init(),
    sourcemaps.write('.'),
  );
});

// plugins css
gulp.task('pluginCss', () => {
  return pipeline(
    gulp.src([
      //node plugin
      paths.node + '/bootstrap/dist/css/bootstrap.css',
      paths.node + '/font-awesome/css/font-awesome.css',
      paths.node + '/@fancyapps/fancybox/dist/jquery.fancybox.css',
      paths.node + '/swiper/dist/css/swiper.min.css',

      //plugin
      paths.vendor + '/*.css',
      paths.vendor + '/*/*.css',
      paths.vendor + '/*/*/*.css'
    ]),
    concat('vendor.css'),
    rename('vendor.min.css'),
    gulp.dest(paths.DistVendor)
  );
});

// js
gulp.task('jshint', () => {
  return pipeline(
    gulp.src(paths.js + '/*.js'),
    jshint({
      esversion: 6
    }),
    jshint.reporter('default')
  );
});

// js
gulp.task('js', () => {
  return pipeline(
    gulp.src(paths.js + '/*.js'),
    sourcemaps.init(),
    babel({
      presets: ['es2015'] // preset 'es2015'javascript5
    }),
    sourcemaps.write('./', { sourceRoot: '../src' }),
    gulp.dest(paths.DistJs)
  );
});

// plugins js
gulp.task('pluginJs', () => {
  return pipeline(
    gulp.src([
      //node plugin
      paths.node + '/@popperjs/dist/umd/popper.js',
      paths.node + '/bootstrap/dist/js/bootstrap.js',
      paths.node + '/@fancyapps/fancybox/dist/jquery.fancybox.js',
      paths.node + '/swiper/dist/js/swiper.min.js',

      //plugin
      paths.vendor + '/*.js',
      paths.vendor + '/*/*.js',
      paths.vendor + '/*/*/*.js'
    ]),
    concat('vendor.js'),
    uglify({
      mangle: false,
      preserveComments: 'all'
    }),
    rename('vendor.min.js'),
    gulp.dest(paths.DistVendor)
  );
});


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

var tasks = ['copy_assets', 'htmlinclude', 'jshint', 'js', 'sass', 'pluginCss', 'pluginJs'];

var main = () => {

  // Run server
  browserSync.init({
    server: "./dist"
  });

  // Run registerd tasks
  gulp.watch([
    paths.views + '/*.html',
    paths.views + '/*/*.html',
    paths.views + '/*/*/*.html'
  ], {
    cwd: './'
  }, ['htmlinclude']);

  //watch scss
  gulp.watch([
    paths.scss + '/*.scss',
    paths.scss + '/*/*.scss',
    paths.scss + '/*/*/*.scss'
  ], {
    cwd: './'
  }, ['sass']);

  //watch js
  gulp.watch([paths.js + '/*.js'], {
    cwd: './'
  }, ['js']);

  // Hot reload
  gulp.watch([
    paths.dist + '/*.html',
    paths.DistCss + '/*.css',
    paths.DistJs + '/*.js'
  ]).on('change', browserSync.reload);

};

gulp.task('default', tasks, main);
gulp.task('watch', tasks, main);