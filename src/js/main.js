(function ($) {

  $(function () { });

  $(window).on('load', function () {

    var $_body = $('body'),
      $_wrapper = $('#wrapper'),
      $_header = $('header'),
      $_ctBody = $('.content-body'),
      $_window = $(this),
      currentScrollTop = 0;

    function init() {
      disableClick();
      getRateElement();
      getMinRateElement();
      link_scrollTop();
      init_facybox();
      // init_swiper();
    }
    init();

    function isIe() {
      var pattern = /Trident\/[0-9]+\.[0-9]+/;

      return pattern.test(navigator.userAgent);
    }

    function isEdge() {
      var pattern = /Edge\/[0-9]+\.[0-9]+/;

      return pattern.test(navigator.userAgent);
    }

    function disableClick() {
      $('.noclick').on('click', function () {
        return false;
      });
    }

    function scrollToPosition(pos, second) {
      $('html, body').animate({
        scrollTop: pos
      }, second * 1000);
    }

    // Resize Height
    function setRateElement(objects, x, y) {
      var w = x || 5;
      var h = y || 3;
      // objects.height(objects.width() * h / w);
      objects.each(function () {
        $(this).height($(this).outerWidth() * h / w);
      });
    }

    function getRateElement() {
      setRateElement($(".imgResize-9-6"), 9, 6);
    }

    // Resize Min Height
    function setMinRateElement(objects, x, y) {
      var w = x || 5;
      var h = y || 3;
      // objects.height(objects.width() * h / w);
      objects.each(function () {
        var $newHeight = (($(this).outerWidth() * h) / w);
        $(this).css('min-height', $newHeight);
      });
    }

    function getMinRateElement() {
      setMinRateElement($(".imgMinResize-9-6"), 9, 6);
    }

    //Click link
    function link_scrollTop() {
      // var $offsetTop = $header - 1;
      $('.js-scroll-trigger a[href*="#"]:not([href="#"])').on('click', function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          $('html, body').animate({
            scrollTop: (target.offset().top)
          }, 1000, "easeInOutExpo");
          return false;
        }
      });
    }

    // init_facybox
    function init_facybox() {

      if (typeof $.fancybox !== 'undefined') {
        console.log('init_facybox');
        $(".fancybox-popup").each(function () {
          $(".fancybox-popup").fancybox({
            toolbar: false,
            smallBtn: true,

            iframe: {
              preload: false
            },
            autoFocus: false,
            btnTpl: {
              smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}">' +
                '<img src="images/icon_close-w.png" alt="" />' +
                '</button>'
            },
            afterShow: function () {
              getRateElement();
            }
          });
        });
      }
    }

    function init_swiper() {
      // Swiper
      if ($('.class-swiper').length) {
        var classSelector = '.class-swiper',
          options = {
            init: false,
            speed: 800,
            slidesPerView: 1, // or 'auto'
            spaceBetween: 0,
            grabCursor: true,
            centeredSlides: true,
            parallax: true,
            observer: true,
            observeParents: true,
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
            pagination: {
              el: '.swiper-pagination',
            },
            on: {
              init: function () {
                getRateElement();
                getMinRateElement();
              },
              slideChangeTransitionStart: function () {
                getRateElement();
                getMinRateElement();
              },
              slideChangeTransitionEnd: function () {
                getRateElement();
                getMinRateElement();
              }
            }
          };

        var sliderSwiper = new Swiper(classSelector, options);
        // Initialize slider
        sliderSwiper.init();
      }
    }

    $(window).on('scroll', function () {
      //Parallax
      var $_width = $(window).width();

      if ($_width > 768) {
        if ($(this).scrollTop() > 400) {
          $('.header.sticky').css('transform', 'translateY(0)');
        } else {
          $('.header.sticky').css('transform', 'translateY(-170%)');
        }
      } else {
        if ($('body').hasClass('logged-in')) {
          if ($(this).scrollTop() > 400) {
            $('.header.sticky').css('transform', 'translateY(-46px)');
          } else {
            $('.header.sticky').css('transform', 'translateY(-170%)');
          }
        } else {
          if ($(this).scrollTop() > 400) {
            $('.header.sticky').css('transform', 'translateY(0)');
          } else {
            $('.header.sticky').css('transform', 'translateY(-170%)');
          }
        }
      }
    });

    $(window).on('resize', function () {
      function init_resize() {
        getRateElement();
        getMinRateElement();
      }
      init_resize();
    });
  });

})(jQuery);
